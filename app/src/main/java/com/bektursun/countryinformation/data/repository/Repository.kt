package com.bektursun.countryinformation.data.repository

import com.bektursun.countryinformation.data.model.region.Region
import io.reactivex.Observable

interface Repository {

    fun getCountryInformationByRegion(region: String): Observable<Result<Region>>
}