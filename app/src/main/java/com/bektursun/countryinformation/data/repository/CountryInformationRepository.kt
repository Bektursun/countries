package com.bektursun.countryinformation.data.repository

import com.bektursun.countryinformation.data.api.AppRemoteSource
import com.bektursun.countryinformation.data.model.region.Region
import io.reactivex.Observable

data class Result<out T>(
    val data: T? = null,
    val error: Throwable? = null
)

fun <T> T.asResult(): Result<T> = Result(data = this, error = null)

open class CountryInformationRepository(private val appRemoteSource: AppRemoteSource) : Repository {

    override fun getCountryInformationByRegion(region: String): Observable<Result<Region>> =
        appRemoteSource.getCountryInformationByRegion(region)
            .flatMap {
                Observable.just(it.asResult())
            }
            .onErrorResumeNext { t: Throwable ->
                return@onErrorResumeNext Observable.just(
                    Result(null, t)
                )
            }
}
