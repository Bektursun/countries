package com.bektursun.countryinformation.data.api

class AppRemoteSource(private val api: CountryInformationApi) {

    fun getCountryInformationByRegion(region: String) = api.getCountriesByRegion(region)
}