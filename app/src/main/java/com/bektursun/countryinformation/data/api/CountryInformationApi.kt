package com.bektursun.countryinformation.data.api

import com.bektursun.countryinformation.data.model.region.Region
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryInformationApi {

    @GET("region/{region}")
    fun getCountriesByRegion(
        @Path("region") region: String
    ): Observable<Region>
}