package com.bektursun.countryinformation.utils

object Constants {

    const val BASE_URL: String = "https://restcountries.eu/rest/v2/"
    const val NETWORK_TIMEOUT: Long = 60L
}