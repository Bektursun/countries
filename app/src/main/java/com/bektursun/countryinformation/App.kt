package com.bektursun.countryinformation

import android.app.Application
import com.bektursun.countryinformation.di.module.networkModule
import com.bektursun.countryinformation.di.module.repositoryModule
import com.bektursun.countryinformation.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(networkModule)
            modules(repositoryModule)
            modules(viewModelModule)
        }
    }
}