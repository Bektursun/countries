package com.bektursun.countryinformation.di.module

import com.bektursun.countryinformation.view.fragment.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(get()) }
}