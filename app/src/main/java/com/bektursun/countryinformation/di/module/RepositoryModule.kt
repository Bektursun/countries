package com.bektursun.countryinformation.di.module

import com.bektursun.countryinformation.data.repository.CountryInformationRepository
import com.bektursun.countryinformation.data.repository.Repository
import org.koin.dsl.module

val repositoryModule = module {
    factory<Repository> {
        CountryInformationRepository(get())
    }
}