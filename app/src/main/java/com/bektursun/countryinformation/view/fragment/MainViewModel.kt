package com.bektursun.countryinformation.view.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bektursun.countryinformation.base.BaseViewModel
import com.bektursun.countryinformation.data.model.region.Region
import com.bektursun.countryinformation.data.repository.Repository
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainViewModel(private val repository: Repository) : BaseViewModel() {

    private val _currentRegion: MutableLiveData<Region> = MutableLiveData()
    val currentRegion: LiveData<Region>
        get() = _currentRegion

    fun getCountryInformationByRegion(region: String) {
        val currentRegion = repository.getCountryInformationByRegion(region)
            .observeOn(Schedulers.io())
            .subscribe({ result -> _currentRegion.postValue(result.data) },
                { e -> Timber.e(e.localizedMessage) })

        compositeDisposable.add(currentRegion)
    }

}
