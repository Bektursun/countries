package com.bektursun.countryinformation.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bektursun.countryinformation.R
import com.bektursun.countryinformation.base.BaseFragment
import com.bektursun.countryinformation.data.model.region.Region
import com.bektursun.countryinformation.databinding.MainFragmentBinding
import com.bektursun.countryinformation.utils.observe
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainFragment : BaseFragment() {


    private lateinit var binding: MainFragmentBinding
    private val viewModel by viewModel<MainViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)

        viewModel.getCountryInformationByRegion("Asia")

        return binding.root
    }

    override fun observeChanges() {
        Timber.i("observeChanges()")
        observe(viewModel.currentRegion, ::getCurrentRegionData)
    }

    private fun getCurrentRegionData(data: Region) {
        println("DATA: $data")
    }

}
